
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsfabrice3

<!-- badges: start -->
<!-- badges: end -->

Le but du package Squirrelsfabrice3 est de reprendre l’étude sur les
Ecureuils dans son intégralité.

## Installation

You can install the development version of squirrelsfabrice3 like so:

``` r
remotes::install_local(path = "~/squirrelsfabrice3_0.0.0.9000.tar.gz")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsfabrice3)
get_message_fur_color(primary_fur_color = "Black")
#> We will focus on Black squirrels
# We will focus on Black squirrels
check_primary_color_is_ok(string = c("Gray", "Cinnamon"))
#> [1] TRUE
# [1] TRUE
```
