---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(assertthat)
library(ggplot2)
library(dplyr)
library(glue)
pkgload::load_all() 
```

<!--
You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# Get a message with a fur color

You can get a message with the fur color of interest with `get_message_fur_color()`.

```{r function-get_message_fur_color}
#' Get a message with the fur color of interest
#'
#' @param primary_fur_color Character. The primary fur color of interest
#' @importFrom glue glue
#' 
#' @return Used for side effect. Outputs a message in the console
#' @export
#'
#' @examples
get_message_fur_color <- function(primary_fur_color) {
  message(glue("We will focus on {primary_fur_color} squirrels"))
}
```

```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")
```

```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"), 
    regexp = "We will focus on Cinnamon squirrels"
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Gray"), 
    regexp = "We will focus on Gray squirrels"
  )
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Black"), 
    regexp = "We will focus on Black squirrels"
  )
  
})
```

# Study the activities of the squirrels given a primary fur color

You can study the activities of the squirrels given a primary fur color with `study_activity()`. 

This function provides 2 elements of analysis: 

+ a filtered table with the repartition of the activities for this primary fur color, 
+ a plot to visualise the activities for this primary fur color.

```{r function-study_activity}
#' Study the activities of the squirrels given a primary fur color
#'
#' @param df_squirrels_act Data frame. A dataset with the activities of the squirrels. This dataset mush have at leat these 4 columns: "age", "primary_fur_color", "activity", "counts".
#' @param col_primary_fur_color Character. The color of the primary fur color of interest. Only the squirrels with this primary fur color will be considered in the analysis.
#' 
#' @importFrom assertthat assert_that
#' @importFrom dplyr filter %>%
#' @importFrom ggplot2 ggplot aes geom_col scale_fill_manual labs
#'
#' @return A list of two named elements. The first one is the filtered table. The second one is the ggplot.
#' @export
#'
#' @examples
study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  assert_that(is.data.frame(df_squirrels_act))
  assert_that(is.character(col_primary_fur_color))
  
  check_squirrel_data_integrity(df_squirrels_act)
  
  table <- df_squirrels_act %>% 
    filter(primary_fur_color == col_primary_fur_color)
  
  graph <- table %>% 
    ggplot() +
    aes(x = activity, y = counts, fill = age) +
    geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))
  
  return(list(table = table, graph = graph))
}
```

```{r example-study_activity}
data(my_dataset)
study_activity(df_squirrels_act = my_dataset, 
               col_primary_fur_color = "Gray")
```

```{r tests-study_activity}
test_that("study_activity works", {
  
  data(my_dataset)
  
  # Everything is ok
  res_study_activity <- study_activity(df_squirrels_act = my_dataset, col_primary_fur_color = "Gray")
  
  expect_equal(object = length(res_study_activity),
               expected = 2)
  
  expect_equal(object = names(res_study_activity),
               expected = c("table", "graph"))
  
  # long dput ----
  expect_equal(object = res_study_activity$table,
               expected = structure(list(age = c("Juvenile", "Adult", "Juvenile", "Juvenile", 
"Adult", "Adult", "Juvenile"), primary_fur_color = c("Gray", 
"Gray", "Gray", "Gray", "Gray", "Gray", "Gray"), activity = c("chasing", 
"climbing", "climbing", "foraging", "running", "chasing", "running"
), counts = c(30L, 456L, 65L, 99L, 513L, 195L, 64L)), class = "data.frame", row.names = c(NA, 
-7L))
                 )
  
  # end of long dput ----
  
  expect_true(inherits(res_study_activity$graph, c("gg", "ggplot")))
  
  # Error : the integrity of the data is not ok
  expect_error(study_activity(df_squirrels_act = iris, col_primary_fur_color = "Gray"))
  
})
```

# Save an output as a csv file

You can save an output as a csv file with `save_as_csv()`. This function can be used, for instance, to save the filtered table created by `study_activity()`.

```{r function-save_as_csv}
#' Save an output as a csv file
#'
#' @param data Data frame. The table to be saved as a csv file.
#' @param path Character. Path where the csv file shloud be saved.
#' @param ... paramètres additionnels (paramètres de write.csv2)
#'
#' @importFrom assertthat assert_that not_empty has_extension is.dir is.writeable
#' @importFrom utils write.csv2
#' 
#' @return The path to the created csv file.
#' @export
#'
#' @examples
save_as_csv <- function(data, path, ...){
  
  assert_that(is.data.frame(data))
  assert_that(not_empty(data))
  assert_that(has_extension(path, ext = "csv"))
  assert_that(is.dir(dirname(path)))
  assert_that(is.writeable(dirname(path)))
  
  write.csv2(x = data, file = path, ...)
  
  return(invisible(path))
}
```

```{r example-save_as_csv}
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = my_dataset, 
  col_primary_fur_color = "Gray"
)

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"))

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)

# avec acces aux autres parametres de write.csv2

my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"),
            row.names = TRUE)
list.files(my_temp_folder)
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))
unlink(my_temp_folder, recursive = TRUE)

```

```{r tests-save_as_csv}
test_that("save_as_csv works", {
  
  # Everything is ok
  my_temp_folder <- tempfile(pattern = "savecsv")
  dir.create(my_temp_folder)
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")), 
               regexp = NA)
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")) %>% browseURL(), 
               regexp = NA)
  
  unlink(my_temp_folder, recursive = TRUE)
  
  # Error
  expect_error(iris %>% save_as_csv("output.xlsx"), 
               regexp = "does not have extension csv")
  
  expect_error(NULL %>% save_as_csv("output.csv"),
               regexp = "data is not a data frame")
  
  expect_error(iris %>% save_as_csv("does/no/exist.csv"), 
               regexp = "does not exist")
  
  expect_error(iris %>% save_as_csv("/usr/bin/output.csv"), 
               regexp = "not writeable")
  
  # test des parametres multiples
  my_temp_folder <- tempfile(pattern = "savecsv")
  dir.create(my_temp_folder)
  
  res_activities_grat_squirrels <- study_activity(
    df_squirrels_act = my_dataset, 
    col_primary_fur_color = "Gray"
  )
  save_as_csv(res_activities_grat_squirrels$table, 
              path = file.path(my_temp_folder, "my_filtered_activities.csv"),
              row.names = T)
  list.files(my_temp_folder)
  res <- read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))
  expect_true(ncol(res) == 5)
  
  unlink(my_temp_folder, recursive = TRUE)
  
})
```

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", 
               vignette_name = "Study the squirrels")
```
